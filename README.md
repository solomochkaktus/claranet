# Claranet

Sollution for Support Engineer Challenge 

**Excercise 1**
Write a bash script that takes three parameters, two strings and a directory name, and substitutes any occurence of the first string with the second string for any file in the directory, recursively;

_Solution_
Bash script with 3 required parameters (Current string, New string, Directory where string must be replaced)

**Excercise 2**
Write a python or bash script that counts the number of script files in a directory subdividing it by the shebang interpreter (i.e. what is after the #! in the first line of a file).

_Solution_
Name of script is task2
Output:




![task2.PNG](./task2.PNG)


**Excercise 3**
Write a cron string that every sunday night create a backup of /home/user folder and send it to a remote server which can be reached using ssh with user@192.168.1.100 (consider private and public key already correctly installed and configured).

_Solution_
Setting should be applied with `crontab -e`

**Excercise 4**

The file structure and configurations are in the task4 folder, here is a short description.
             
        `wp-admin`: 755
        `wp-content`: 755
                wp-content/themes: 755
                wp-content/plugins: 755
                wp-content/uploads: 755
        `wp-config.php`: 644
        `.htaccess`: 644
        `All other files` – 644

![image.png](./image.png)
